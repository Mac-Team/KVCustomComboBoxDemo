//
//  ViewController.m
//  KVCustomComboBoxDemo
//
//  Created by kevin on 2017/2/16.
//  Copyright © 2017年 kevin. All rights reserved.
//

#import "ViewController.h"
#import <Masonry.h>
#import "KVCustomComboBox.h"

typedef KVCustomComboBox *(^CreatKVCustomComboBoxBlock)(void);

@interface ViewController ()

@property(nonatomic, copy)CreatKVCustomComboBoxBlock creatComboBoxBlock;

@end

@implementation ViewController {
    KVCustomComboBox    *kvComboBox;
    NSButton            *checkBtn;
    BOOL                isChecked;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initSubview];
    [self testData];
}

-(void)initSubview {
    
    self.creatComboBoxBlock = ^() {
        NSMutableArray *objList = [[NSMutableArray alloc] init];
        
        BOOL succ = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([KVCustomComboBox class]) owner:nil topLevelObjects:&objList];
        
        if ( succ ) {
            for (id obj in objList) {
                if ( [obj isKindOfClass:[KVCustomComboBox class]] ) {
                    KVCustomComboBox *retComboBox = (KVCustomComboBox *)obj;
                    return retComboBox;
                }
            }
        }
        
        return [KVCustomComboBox new];
    };
    
    if ( self.creatComboBoxBlock ) {
        kvComboBox =  self.creatComboBoxBlock();
        
        if ( kvComboBox ) {
            
            [self.view addSubview:kvComboBox];
            
            [kvComboBox mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(self.view);
                make.top.equalTo(self.view).offset(20);
                make.width.equalTo(@kComboBoxWidth);
                make.height.equalTo(@(kComboBoxHeight * 5));
            }];
        }
    }
    
    checkBtn = [[NSButton alloc] init];
    [checkBtn setButtonType:NSSwitchButton];
    checkBtn.bezelStyle = NSRoundedBezelStyle;
    checkBtn.title = @"隐藏账号";
    [checkBtn  setTarget:self];
    [checkBtn setAction:@selector(onButtonClicked:)];
    [self.view addSubview:checkBtn];
    
    [checkBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(kvComboBox.mas_bottom).offset(20);
        make.left.equalTo(kvComboBox);
        make.width.equalTo(@80);
        make.height.equalTo(@20);
    }];
    
    NSButton *addBtn = [NSButton new];
    [self.view addSubview:addBtn];
    [addBtn setTitle:@"保存账号"];
    [addBtn setTarget:self];
    [addBtn setAction:@selector(onAddBtnClicked:)];
    
    [addBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(checkBtn);
        make.left.equalTo(checkBtn.mas_right).offset(20);
        make.width.equalTo(@60);
        make.height.equalTo(@20);
    }];
}

-(void)testData {
    kvComboBox.currentValue = [[NSMutableString alloc] initWithString:@"111111111111"];
    kvComboBox.dataList = [NSMutableArray arrayWithArray:[self getDataList]];
}

-(NSArray *)getDataList {
    return @[@"111111111111", @"222222222222", @"333333333333", @"444444444444", @"555555555555", @"666666666666"];
}

-(void)onAddBtnClicked:(id)sender {
    [kvComboBox addData:kvComboBox.currentValue];
}

-(void)onButtonClicked:(id)sender {
    isChecked = !isChecked;
    [kvComboBox setTextSecure:isChecked];
}

//- (void)viewDidMoveToWindow {
//    [self.view addTrackingRect:[self.view bounds] owner:self userData:nil assumeInside:NO];
//}
//
//-(void)mouseEntered:(NSEvent *)theEvent {
//    NSLog(@"mouseEctered");
//}
//
//-(void) mouseExited:(NSEvent *)theEvent {
//    NSLog(@"Exit");
//}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}


@end
