//
//  KVCustomComboBox.h
//  KVComboBoxDemo
//
//  Created by oupeng on 17/2/13.
//  Copyright © 2017年 oupeng. All rights reserved.
//

#import <Cocoa/Cocoa.h>

/**
 ComboBox 尺寸
 */
#define kComboBoxWidth      240
#define kComboBoxHeight     30

@interface KVCustomComboBox : NSView

@property(nonatomic, strong)NSFont          *textFont;      /**< 输入框字体 */
@property(nonatomic, strong)NSColor         *textColor;     /**< 输入框文本颜色 */
@property(nonatomic, strong)NSColor         *textBgColor;   /**< 输入框背景颜色 */
@property(nonatomic, assign)BOOL            textBordered;   /**< 输入框是否显示边框 */
@property(nonatomic, assign)BOOL            textSecure;     /**< 是否显示密文 */

@property(nonatomic, strong)NSFont          *listFont;      /**< 列表字体 */
@property(nonatomic, strong)NSColor         *listColor;     /**< 列表文本颜色 */
@property(nonatomic, strong)NSColor         *listBgColor;   /**< 列表背景颜色 */
@property(nonatomic, assign)BOOL            hasDeleteBtn;   /**< 列表是否有删除按钮 */

@property(nonatomic, strong)NSMutableString *currentValue;  /**< 当前值 */

@property(nonatomic, strong)NSMutableArray  *dataList;      /**< 数据列表 */

-(void)addData:(NSString *)data;
-(void)deleteDataAtIndex:(NSUInteger)index;
-(void)deleteAllData;

@end
